.. GNU Toolchain Infrastructure Technical Advisory Council documentation master file, created by
   sphinx-quickstart on Wed Sep 28 16:55:11 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GNU Toolchain Infrastructure Technical Advisory Council
===================================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   2022-08-24

Meeting minutes
---------------
* :doc:`2022-08-24`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
